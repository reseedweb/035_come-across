			<nav>
                <div id="gNavi" class="wrapper">
                    <ul class="dynamic clearfix">
                        <li>
							<a href="<?php bloginfo('url'); ?>/service">
								<span class="first">サービス</span>
								<span class="second">service</span>
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/product">
								<span class="first">製品紹介</span>
								<span class="second">products</span>
							</a>
						</li>                            
						<li>
							<a href="<?php bloginfo('url'); ?>/reason">
								<span class="first">選ばれる理由</span>
								<span class="second">reason</span>
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/flow">
								<span class="first">ご注文の流れ</span>
								<span class="second">flow</span>
							</a>
						</li>    
						<li>
							<a href="<?php bloginfo('url'); ?>/faq">
								<span class="first">よくある質問</span>
								<span class="second">Q&amp;A</span>
							</a>
						</li> 
						 <li>
							<a href="<?php bloginfo('url'); ?>/estimate">
								<span class="first">お見積り</span>
								<span class="second">estimate</span>
							</a>
						</li> 					
                    </ul>
                </div>
            </nav>
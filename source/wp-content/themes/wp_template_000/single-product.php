<?php get_header(); ?>
<div class="primary-row clearfix">
	<h3 class="h3-title">製品紹介</h3>
	<?php if(have_posts()): while(have_posts()) : the_post(); ?>
	<ul class="product-categorys clearfix"><!-- begin product-tabs-->
		<li class="first-child"><a href="<?php bloginfo('url'); ?>/cat-product/mens">Men's</li>
		<li class="second-child"><a href="<?php bloginfo('url'); ?>/cat-product/ladys">Lady's</li> 
		<li class="last-child"><a href="<?php bloginfo('url'); ?>/cat-product/kids">Kid's</li>		
	</ul><!-- end product-tabs -->
	<div class="product-detail-content clearfix">
		<div class="product-detail-text">
			<h4 class="product-dtitle"><?php the_title(); ?></h4>
			<div class="product-dcate-text1">商品カテゴリ</div><div class="product-dcate-text2"><?php @the_terms(get_the_id(), 'cat-product'); ?></div>
			<div class="product-dtext">
				<?php the_content(); ?>
			</div>
			<div id="subalbum" class="product-detail-thumbimg">
					<ul class="clearfix">
						<li>										
							<?php if( get_field('product_image1') ): ?>
								<a href="<?php the_field('product_image1'); ?>"><img src="<?php the_field('product_image1'); ?>" /></a>
							<?php else :?>
								<a href="<?php bloginfo('template_url'); ?>/img/content/product_detail_img.jpg"><img src="<?php bloginfo('template_url'); ?>/img/content/product_detail_img.jpg" alt="product" /></a>					
							<?php endif; ?>	
						</li>
						<li>										
							<?php if( get_field('product_image2') ): ?>
								<a href="<?php the_field('product_image2'); ?>"><img src="<?php the_field('product_image2'); ?>" /></a>
							<?php else :?>
								<a href="<?php bloginfo('template_url'); ?>/img/content/product_detail_img.jpg"><img src="<?php bloginfo('template_url'); ?>/img/content/product_detail_img.jpg" alt="product" /></a>					
							<?php endif; ?>	
						</li>					
					</ul>
				</div>
		</div><!-- end product-detail-text -->
		
		<div id="mainalbum" class="product-detail-img">
			<?php if( get_field('product_image1') ): ?>
				<img src="<?php the_field('product_image1'); ?>" />
			<?php else :?>
				<img src="<?php bloginfo('template_url'); ?>/img/content/product_detail_img.jpg" alt="product" />					
			<?php endif; ?>			
		</div><!-- end product-detail-img -->		
	</div><!-- end product-detail-content -->
		
	<table class="product-table-content">
		<tr>
			<th>生地の組成</th>
			<td><?php echo get_field('material', get_the_id()); ?></td>
		</tr>
		<tr>
			<th>生地の組成</th>
			<td>
				<p><span class="product-table-note1">長 傘</span><?php echo get_field('style_length', get_the_id()); ?></p>
				<p class="pt10"><span class="product-table-note2">ミニ傘</span><?php echo get_field('style_mini', get_the_id()); ?></p>
			</td>
		</tr>
		<tr>
			<th>生地の組成</th>
			<td>
				<p><span class="product-table-note1">長 傘</span><?php echo get_field('hand_length', get_the_id()); ?></p>
				<p class="pt10"><span class="product-table-note2">ミニ傘</span><?php echo get_field('hand_mini', get_the_id()); ?></p>
			</td>
		</tr>
	</table>
	  
	<div class="primary-row clearfix">	    		
		<div class="product-detail-btn">			
			<?php previous_post_link('%link', '« %title'); ?>			
		</div><!-- ./product-detail-bg -->					
	</div><!-- end primary-row -->	
	<?php endwhile;endif; ?>
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<h3 class="h3-title">関連製品紹介</h3>
	<?php 	 
	$custom_taxterms = wp_get_object_terms( $post->ID, 'cat-product', array('fields' => 'ids') );	
	$args = array(
	'post_type' => 'product',
	'post_status' => 'publish',
	'posts_per_page' => 6,
	'orderby' => 'rand',
	'tax_query' => array(
		array(
			'taxonomy' => 'cat-product',
			'field' => 'id',
			'terms' => $custom_taxterms
		)
	),
	'post__not_in' => array ($post->ID),
	);
	$related_items = new WP_Query( $args );		?>	
	<?php echo '<div class="product-list-group">';?>
	<?php if ($related_items->have_posts()) :?>
	<?php $i = 0;?>
	<?php while ( $related_items->have_posts() ) : $related_items->the_post();?>			
		<?php $i++; ?>
		<?php if($i%3 == 1) : ?>
		<div class="product-list-row clearfix">		
		<?php endif; ?>
			<div class="product-list-col product-list-col244">
				<div class="image">
					<?php echo get_the_post_thumbnail($post->ID,'medium'); ?>
				</div><!-- end image -->
				<div class="title">
					<?php echo $post->post_title; ?>
				</div><!-- end title -->    
				<div class="product-list-btn clearfix">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">商品詳細はこちら<i class="fa fa-caret-right"></i></a>
				</div>	
			</div><!-- end product-list-col -->
			<?php if($i%3 == 0 || $i == count($args) ) : ?>				
		</div>		
		<?php endif; ?>						
	<?php endwhile;endif;?>
	<?php echo '</div>';?>
	<?php wp_reset_postdata();?>
</div>
 
<script type="text/javascript">
	$(document).ready(function(){
		$('#subalbum a').click(function(){
			$('#mainalbum img').hide();
			$('#mainalbum img').attr('src',$(this).attr('href'));
			$('#mainalbum img').fadeIn();
			return false;
		});
	});
</script>
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>


<?php get_header(); ?>		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">サービスとOEM</h3>
		<div class="service-top">
			<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img1.png" alt="service" />
		</div>
		<h4 class="service_top-title">企画から試作・生産までトータルでOEM を承っております。</h4>
		<div class="ln2em">
			<p>各種イベントや企業のプロモーション用として大好評！また、プレゼントや記念品としても最適です！</p>
			<p>さらに、同人イベントなどでの使用も可能です。デザイン制作のご相談も承りますので、お気軽にお問い合わせ下さい。</p>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">傘総本家のOEM の特徴</h3>
		<div class="message-right message-300 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img2.jpg" alt="service" />
			</div>
			<div class="text ln2em">
				<p>当社では、お客様のご要望に応じて、<br />傘、レイングッズをご提供いたします。</p>
				<p>私達は、中国に自社工場を持ち、企画からデザイン、<br />生産に至るまでのトールなご依頼から、生産のみの<br />スポット的なご依頼まで、傘の制作全般にご対応いたします。</p>
				<p>小ロット対応も可能ですので、お気軽にご相談ください。</p>
				<p class="pt20">主に、下記の納入実績がございます。</p>
			</div>
		</div><!-- end message-300 -->
		<ul class="service-list clearfix">
			<li>アパレルブランド様向けOEM商品</li>
			<li>セレクトショップ様向けOEM商品</li>
			<li>通信販売様向けOEM商品</li>
			<li>中級～高級品ノベルティOEM商品</li>
			<li>特殊な傘のOEM商品</li>
			<li>難解な傘のOEM商品</li>
			<li>テレビや映画に使用するワンオフ製品（可能な範囲でお手伝いします）</li>
		</ul>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="service-content-btn">
			<a href="<?php bloginfo('url'); ?>/factory">
				<img src="<?php bloginfo('template_url'); ?>/img/content/service_btn1.jpg" alt="service" />
			</a>
		</div>
		<p class="pt20">傘総本家の基本的なOEM製造条件と致しましては、以下の通りです。</p>
		<table class="service-table">
			<tr>
				<th>最低ロット</th>
				<td>240本～</td>
			</tr>
			<tr>
				<th>納期</th>
				<td>正式オーダーより4～5ヶ月後  <span class="service-stext">※シーズンにより異なります。繁忙期は納期が長くなります。</span></td>
			</tr>
		</table><!-- ./service-table -->
		<div class="service-content-btn">
			<a href="<?php bloginfo('url'); ?>/contact">
				<img src="<?php bloginfo('template_url'); ?>/img/content/service_btn2.jpg" alt="service" />
			</a>
		</div><!-- service-content-btn -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">傘のOEM 生産の流れ</h3>
		<div class="service-process-content clearfix">
			<div class="service-step-process clearfix">
				<h4 class="service-process-title"><span class="service-process-num">Step1</span>ご要望の確認</h4>
				<div class="service-process-text">ご予算・数・納期・デザインなどのヒアリングを行います。</div>
			</div><!-- ./service-step-process -->
			
			<div class="service-step-process mt20 clearfix">
				<h4 class="service-process-title"><span class="service-process-num">Step2</span>企画・デザインのご提案</h4>
				<div class="service-process-text">要望に応じて当社からの提案をさせて頂きます。</div>
			</div><!-- ./service-step-process -->
			
			<div class="service-step-process mt20 clearfix">
				<h4 class="service-process-title"><span class="service-process-num">Step3</span>仕様確認・お見積もり</h4>
				<div class="service-process-text">製作する仕様と内容を確認、それを基にしたお見積りの提示。</div>
			</div><!-- ./service-step-process -->
			
			<div class="service-step-process mt20 clearfix">
				<h4 class="service-process-title"><span class="service-process-num">Step4</span>サンプル作成・最終確認</h4>
				<div class="service-process-text">
					<p>お見積りにご了承いただけましたら、サンプルを作成いたします。</p>
					<p>また、生産を開始してよいかをここで最終確認します。</p>
				</div>
			</div><!-- ./service-step-process -->
			
			<div class="service-step-process mt20 clearfix">
				<h4 class="service-process-title"><span class="service-process-num">Step5</span>生産開始</h4>
				<div class="service-process-text">ご提示したサンプルを元に量産体制に入ります。</div>
			</div><!-- ./service-step-process -->
			
			<div class="service-step-process mt20 clearfix">
				<h4 class="service-process-title"><span class="service-process-num">Step6</span>発送・納品</h4>
				<div class="service-process-text">
					<p>製造期間：４ヶ月〜5ヶ月程度</p>
					<p>※シーズンにより異なります。繁忙期（11〜6月）は納期が長くなります。</p>
					<p>工場出荷から弊社倉庫に入るまで10日～13日前後かかります。</p>
				</div>
			</div><!-- ./service-step-process -->		
		</div><!-- ./service-process-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">傘ができるまで</h3>
		<p>洋傘は骨や生地などのパーツが別々に製造され、それを組み立てて作っていきます。</p>
		<p>組み立ては手作業なので、傘職人の腕が製品の質に大きく影響します。</p>
		<p>弊社は日本の傘職人の匠が直接工場で指導をしているので、高品質の傘をみなさまにお届けすることができます。</p>
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="service-content clearfix">
			<h4 class="service-title">
				<span class="title-step">STEP1</span>
				<span class="title-text">生地の断裁</span>
			</h4>
			<div class="service-info">
				<div class="message-right message-264 clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img3.jpg" alt="service" />
					</div>
					<div class="text">
						<p>傘のサイズやフォルムに合わせて木で作った型を使って、<br />生地を三角形に裁断します。</p>
						<p>出来上がった時に美しいフォルムに成るか否か。</p>
						<p>職人の技が光ります。</p>
						<p>この裁断された生地を「小間( こま)」と呼びます。</p>
					</div>
				</div><!-- end message-264 -->
			</div><!-- ./service-info -->
		</div><!-- end service-content -->
		<div class="service-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="service" />
		</div><!-- service-arrow -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="service-content clearfix">
				<h4 class="service-title">
					<span class="title-step">STEP2</span>
					<span class="title-text">中縫い</span>
				</h4>
				<div class="service-info clearfix">
					<div class="service-step2-img1">
						<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img4.jpg" alt="service" />
						<p class="mt10"><span class="service-num">1</span><span class="service-ntext">ミシンで骨の数に合わせて「小間( こま)」を<br />縫い合わせて傘の布地( カバー) を作ります。</span></p>
					</div>
					<div class="service-step2-img2">
						<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img5.jpg" alt="service" />
						<p class="mt10"><span class="service-num">2</span>縫い糸は雨漏りを防止する為に撥水加工を施した<br />糸を使用します。</p>
					</div>
				</div><!-- ./service-info -->
			</div><!-- end service-content -->
			<div class="service-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="service" />
			</div><!-- service-arrow -->
		</div><!--- ./primary-row -->
		
		<div class="primary-row clearfix">
			<div class="service-content clearfix">
				<h4 class="service-title">
					<span class="title-step">STEP3</span>
					<span class="title-text">穴かがり</span>
				</h4>
				<div class="service-info">
					<div class="message-right message-264 clearfix">
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img6.jpg" alt="service" />
						</div>
						<div class="text">
							<p>カバーの天頂部に中棒を通すための穴を作ります。</p>
							<p>中棒の太さに合わせて専用のかがり棒を穴に通して小間と<br />小間がほつれないようにかがります。</p>
						</div>
					</div><!-- end message-264 -->
				</div><!-- ./service-info -->
			</div><!-- end service-content -->
			<div class="service-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="service" />
			</div><!-- service-arrow -->
		</div><!-- ./primary-row -->
		
		<div class="primary-row clearfix">
			<div class="service-content clearfix">
				<h4 class="service-title">
					<span class="title-step">STEP4</span>
					<span class="title-text">ネーム付</span>
				</h4>
				<div class="service-info">
					<div class="message-right message-264 clearfix">
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img7.jpg" alt="service" />
						</div>
						<div class="text">
							<p>カバーにたたんだ時に傘をまとめる胴ネームを縫い付けます。</p>
						</div>
					</div><!-- end message-264 -->
				</div><!-- ./service-info -->
			</div><!-- end service-content -->
			<div class="service-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="service" />
			</div><!-- service-arrow -->
		</div><!-- ./primary-row -->
		
		<div class="primary-row clearfix">
			<div class="service-content clearfix">
				<h4 class="service-title">
					<span class="title-step">STEP5</span>
					<span class="title-text">ネーム付</span>
				</h4>
				<div class="service-info">
					<div class="message-group message-col226"><!-- begin message-group -->
						<div class="message-row clearfix"><!--message-row -->
							<div class="message-col">								
								<div class="image">
									<iframe width="226" height="178" src="https://www.youtube.com/embed/YagZumW3BMU" frameborder="0" allowfullscreen></iframe>
								</div><!-- end image -->    
								<div class="text">
									<p class="mt10"><span class="service-num">1</span><span class="service-ntext">出来上がったカバーに露先を<br />取り付けます。</span></p>
								</div>            
							</div><!-- end message-col -->
							<div class="message-col">								
								<div class="image">
									<iframe width="226" height="178" src="https://www.youtube.com/embed/rhVpNLqjsig" frameborder="0" allowfullscreen></iframe>
								</div><!-- end image -->    
								<div class="text">
									<p class="mt10"><span class="service-num">2</span><span class="service-ntext">骨にカバーを取り付け、露先<br />を親骨に入れます。</span></p>
								</div>            
							</div><!-- end message-col -->
							<div class="message-col">								
								<div class="image">
									<iframe width="226" height="178" src="https://www.youtube.com/embed/UeXSWuWdVTE" frameborder="0" allowfullscreen></iframe>
								</div><!-- end image -->    
								<div class="text">
									<p class="mt10"><span class="service-num">3</span><span class="service-ntext">そして親骨にカバーをずれな<br />いように専用の糸を使って<br />しっかりと取り付けます。</span></p>
								</div>            
							</div><!-- end message-col -->
						</div><!-- end message-row -->
					</div><!-- end message-group -->
				</div><!-- ./service-info -->
			</div><!-- end service-content -->
			<div class="service-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="service" />
			</div><!-- service-arrow -->
		</div><!-- ./primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="service-content clearfix">
				<h4 class="service-title">
					<span class="title-step">STEP6</span>
					<span class="title-text">検査・仕上げ</span>
				</h4>
				<div class="service-info clearfix">
					<div class="service-step2-img1">
						<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img8.jpg" alt="service" />
						<p class="mt10"><span class="service-num">1</span><span class="service-ntext">ミシンで骨の数に合わせて「小間( こま)」を<br />縫い合わせて傘の布地( カバー) を作ります。</span></p>
					</div>
					<div class="service-step2-img2">
						<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img9.jpg" alt="service" />
						<p class="mt10"><span class="service-num">2</span><span class="service-ntext">縫い糸は雨漏りを防止する為に撥水加工を施した<br />糸を使用します。</span></p>
					</div>
				</div><!-- ./service-info -->
			</div><!-- end service-content -->
			<div class="service-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="service" />
			</div><!-- service-arrow -->
		</div><!--- ./primary-row -->
		
		<div class="primary-row clearfix">
			<div class="service-content clearfix">
				<h4 class="service-title">
					<span class="title-step">STEP7</span>
					<span class="title-text">完成</span>
				</h4>
				<div class="service-info">
					<div class="message-right message-264 clearfix">
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img10.jpg" alt="service" />
						</div>
						<div class="text">
							<p>カバーにたたんだ時に傘をまとめる胴ネームを縫い付けます。</p>
						</div>
					</div><!-- end message-264 -->
				</div><!-- ./service-info -->
			</div><!-- end service-content -->			
		</div><!-- ./primary-row -->	
	</div><!-- end primary-row -->
	
	
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
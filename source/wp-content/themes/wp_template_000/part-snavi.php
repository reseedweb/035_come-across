<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<aside id="sideNavi"><!-- begin sideNavi -->		
		<ul class="sideNavi-content">
			<li><a href="<?php bloginfo('url'); ?>/">ホーム<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/service">サービス<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/product">製品紹介<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/reason">選ばれる理由<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/flow">ご注文の流れ<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/estimate">お見積り<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/factory">工場紹介<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/blog">ブログ<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/company">運営会社<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー<i class="fa fa-play"></i></a></li>			
		</ul>
	</aside><!-- end sideNavi -->
</div><!-- end sidebar-row -->












<?php get_header(); ?>		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="reason-content-top">
			<div class="reason-text">
				<h2 class="reason-top-title">傘総本家が選ばれる<span class="reason-title-clr">３つの理由</span></h2>
				<p>私たち「傘総本家」は、<br />
				<span class="reason-top-note">「NOとは言わない傘づくり！お客様の要望をカタチにします。」</span><br />を合言葉に 傘製作・OEMを行っております。</p>
				<p>「この様な傘は可能だろうか？」「特殊なプリントで依頼したい。」</p>
				<p>「オリジナルの子供傘を作りたい」といったお客様の声にお応えしたい。</p>
				<p>そういった想いから、当社は自社で生産できる体制を整えているとともに、<br />長年の経験から培ってきた 知識・技術に裏打ちされたサポート体制も整えております。</p>
			</div>
			<div class="reason-img">
				<img src="<?php bloginfo('template_url'); ?>/img/content/reason_top_img2.png" alt="reason" />
			</div>
		</div>
	</div><!-- end primary-row -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">傘・日傘の企画・デザインから生産まで一貫生産</h3>		
		<div class="message-right message-300 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_img1.jpg" alt="reason" />
			</div>
			<div class="text">
				<p>傘総本家では、傘・日傘の企画からデザイン、そして生産まで<br /><span class="reason-clrpink">すべての工程を自社のみ</span>で行っています。</p>
				<p>初めてのお客様で傘の知識がなくてもご安心ください。</p>
				<p>過去の豊富な製作実績から多様なパターンの傘をご提案させて<br />いただきますので、お気軽にご相談ください。</p>
			</div>
		</div><!-- end message-300 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">小ロットは、240本から対応</h3>		
		<div class="message-right message-300 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_img2.jpg" alt="reason" />
			</div>
			<div class="text">
				<p>当社ではこれまで様々な企業や施設のオリジナル傘 や痛傘を制作して参りました。法人のお客様はもちろん個人のお客様にも気軽にオリジナル傘をご注文頂ける様、他にはない小ロット対応でなんと<span class="reason-clrpink">240本から対応</span>させていただきます。</p>
				</p>もちろんこれ以上のオリジナル傘・弊社製品の注文も受け付けておりますのでお気軽にご相談ください。</p>
			</div>
		</div><!-- end message-300 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">自社工場で熟練の職人が製作するから高品質</h3>		
		<div class="message-right message-300 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_img3.jpg" alt="reason" />
			</div>
			<div class="text">
				<p>傘総本家は<span class="reason-clrpink">日本で唯一中国に自社工場を持ち、人、技術、素材、<br />全てを「メイドインジャパン」の品質でご提供</span>しています。</p>
				<p>中国工場にも、日本から担当を派遣することで、同じ技術力で日々傘を製作しています。</p>
				<p>細かな仕様のご要望から小ロット対応まで、自社工場ならではのOEM を行っております。</p>
			</div>
		</div><!-- end message-300 -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
 <?php get_header(); ?>		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<p>オリジナルの傘を製作するに当たり、傘は大きく2つの用途に分かれます。</p>
		<p class="mb10">主に雨を防ぐ用途として一般的な強い防水加工が施された「雨傘」、そしてもう一方は日傘としても兼用できる「晴雨兼用傘」の2つに分類されます。</p>
		<h3 class="h3-title">傘の用途別で選ぶ</h3>		
		<h4 class="use-title">雨傘</h4>
		<p><img src="<?php bloginfo('template_url'); ?>/img/content/use_content_img1.jpg" alt="use" /></p>
		<div class="use-content clearfix">
			<div class="use-content-left">
				<h5 class="use-subtitle">こんな方にもおすすめ</h5>
				<p>・少しでもお安く作りたいならこちらです</p>
				<p>・防水性に優れた傘を作りたい方</p>
			</div>
			<div class="use-content-right">
				<h5 class="use-subtitle">特微</h5>
				<p>雨を避けるために少し大きめに作られています。</p>
				<p>また、日傘としても使用することも可能です。</p>	
			</div>
		</div>
		<h4 class="use-title">晴雨兼用傘</h4>
		<p><img src="<?php bloginfo('template_url'); ?>/img/content/use_content_img2.jpg" alt="use" /></p>
		<div class="use-content clearfix">
			<div class="use-content-left">
				<h5 class="use-subtitle">こんな方にもおすすめ</h5>
				<p>・デザインにこだわりたい方に</p>
				<p>・紫外線をカットされたい方に</p>
				<p>・雨の日でも気軽に使いたい方に</p>
			</div>
			<div class="use-content-right">
				<h5 class="use-subtitle">特微</h5>
				<p>防水効果が施されない代わりに紫外線カット効果が高めです。</p>
				<p>また、雨傘に比べると少し小さめに作られています。</p>
				<p>晴れの日に使用する傘ですが、防水加工をしているので、にわか雨等に対応出来ます。</p>	
			</div>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<div class="use-content-btn clearfix">
			<a href="<?php bloginfo('url'); ?>/frame"><img src="<?php bloginfo('template_url'); ?>/img/content/use_content_btn.jpg" alt="use" /></a>
		</div><!-- hand-btn -->
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
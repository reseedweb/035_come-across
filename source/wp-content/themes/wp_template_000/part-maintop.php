<section id="maintop">
	<div class="maintop-content wrapper clearfix">
		<div class="maintop-left">			
			<p><img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/maintop_content_img1.jpg" /></p>
		</div>
		<div class="maintop-right">
			<p>
				<a href="<?php bloginfo('url'); ?>/estimate">
					<img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/maintop_content_img2.jpg" />
				</a>
			</p>
			<p class="maintop-right2">
				<a href="<?php bloginfo('url'); ?>/product_list">
					<img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/maintop_content_img3.jpg" />
				</a>
			</p>
		</div>
	</div>
</section>

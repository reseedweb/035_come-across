<?php get_header(); ?>		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">ご注文の流れ</h3>
		<div class="flow-content clearfix">
			<h4 class="flow-title">
				<span class="title-step">STEP1</span>
				<span class="title-text">お問い合わせ・ヒアリング</span>
			</h4>
			<div class="flow-info">
				<p>まずは、お電話・メールにて弊社までご連絡ください。</p>
				<p>ちょっとしたご質問、ご相談でも構いません。</p>
				<p>専門のスタッフが対応させていただきます。</p>
				<div class="step1-top-img"><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.png" alt="flow" /></div>
				<div class="flow-step1 clearfix">
					<div class="flow-text">
						<h5 class="fstep1-title"><i class="fa fa-circle-o"></i>こんな方におすすめ</h5>
						<p>お問い合わせフォームからご注文される商品をご入力ください。</p>
						<p>お問い合わせ後、弊社スタッフより連絡させていただきます。</p>
					</div>
					<a href="<?php bloginfo('url'); ?>/product" class="flow-btn">
						<i class="fa fa-play"></i>製品を見る
					</a>											
				</div><!-- flow-step1 -->
				
				<div class="flow-step1 clearfix">
					<div class="flow-text">
						<h5 class="fstep1-title"><i class="fa fa-circle-o"></i>オリジナル傘・OEMについて</h5>
						<p>下記のお見積りフォームから、ご要望を記載してお送りください。</p>						
					</div>
					<a href="<?php bloginfo('url'); ?>/estimate" class="flow-btn mb10">
						<i class="fa fa-play"></i>オリジナル傘・OEMのお見積り
					</a>											
				</div><!-- flow-step1 -->
				
				<div class="flow-part-con">	
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_part_con_bg.png" alt="flow" />			
					<div class="flow-part-con-btn">                   
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/flow_part_con_btn.jpg" alt="flow" />
						</a>
					</div>
				</div><!-- ./top-part-con -->
			</div><!-- end flow-info -->
		</div><!-- end flow-content -->
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="flow" />
		</div><!-- flow-arrow -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="flow-content clearfix">
			<h4 class="flow-title">
				<span class="title-step">STEP2</span>
				<span class="title-text">見積書の提出</span>
			</h4>
			<div class="flow-info">
				<div class="flow-right-first message-264 clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.jpg" alt="flow" />
					</div>
					<div class="text">
						<p>担当者より、電話・FAX・メールのいずれかの方法で、<br />一両日中にご連絡差し上げます。</p>
						<p>お問い合わせの内容によっては、お見積りにお時間を<br />いただく場合もございますのでご了承下さい。</p>
						<p>データからの製作をご希望の場合、お問い合わせの<br />段階で、イメージや素材などを送付して下さい。</p>
						<p>ご希望のデザインによって、お見積り内容が変わりますので、<br />あらかじめ送付していただいたほうがスムーズです。</p>
						<p>お見積り内容をご確認いただいた上で、ご注文の連絡をお願い致します。</p>
					</div>
				</div><!-- end message-264 -->
			</div><!-- ./flow-info -->
		</div><!-- end flow-content -->
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="flow" />
		</div><!-- flow-arrow -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="flow-content clearfix">
			<h4 class="flow-title">
				<span class="title-step">STEP3</span>
				<span class="title-text">サンプルの作成</span>
			</h4>
			<div class="flow-info">
				<div class="flow-right-first message-264 clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.jpg" alt="flow" />
					</div>
					<div class="text">
						<p>お見積もりで問題がなければサンプル製作に入ります。</p>
						<p>サンプル完成後に修正点等ありましたらお気軽にお申し付け下さい。</p>
						<p class="flow-text-clr">※製作に約２～３週間かかりますのでご了承下さい。</p>
					</div>
				</div><!-- end message-264 -->
			</div><!-- ./flow-info -->
		</div><!-- end flow-content -->
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="flow" />
		</div><!-- flow-arrow -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="flow-content clearfix">
			<h4 class="flow-title">
				<span class="title-step">STEP4</span>
				<span class="title-text">傘の量産</span>
			</h4>
			<div class="flow-info">
				<div class="flow-right-first message-264 clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.jpg" alt="flow" />
					</div>
					<div class="text">
						<p>サンプルが出来ましたらそれを元に生産に入ります。</p>
						<p>熟練の職人が１本１本心を込めて丁寧にお作りします。</p>
						<p>工場の込み具合で多少お時間を頂く場合があります。</p>
						<p class="flow-text-clr">納期：約２ヶ月～３ヶ月</p>
					</div>
				</div><!-- end message-264 -->
			</div><!-- ./flow-info -->
		</div><!-- end flow-content -->
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="flow" />
		</div><!-- flow-arrow -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="flow-content clearfix">
			<h4 class="flow-title">
				<span class="title-step">STEP5</span>
				<span class="title-text">検品作業</span>
			</h4>
			<div class="flow-info">
				<div class="flow-right-first message-264 clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.jpg" alt="flow" />
					</div>
					<div class="text">
						<p>傘生地部が破けていたり汚れていたりしてないか？</p>
						<p>傘骨が曲がっていないか？</p>
						<p>ロゴは指定位置に印刷されているか？</p>
						<p>などの検品作業を行います。</p>
						<p>この時に下げ札等ありましたらお付けします。</p>
					</div>
				</div><!-- end message-264 -->
			</div><!-- ./flow-info -->
		</div><!-- end flow-content -->
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="flow" />
		</div><!-- flow-arrow -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="flow-content clearfix">
			<h4 class="flow-title">
				<span class="title-step">STEP6</span>
				<span class="title-text">納品</span>
			</h4>
			<div class="flow-info">
				<div class="flow-right-first message-264 clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img6.jpg" alt="flow" />
					</div>
					<div class="text">
						<p>すべての工程が完了の後<br />お客様のご希望の日程、納品先に商品をお送りします。</p>
					</div>
				</div><!-- end message-264 -->
			</div><!-- ./flow-info -->
		</div><!-- end flow-content -->		
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
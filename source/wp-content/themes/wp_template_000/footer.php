
                    </main><!-- end primary -->
                    <aside class="sidebar"><!-- begin sidebar -->
                        <?php if(is_page('blog') || is_category() || is_single()) : ?>
                            <?php
                            $queried_object = get_queried_object();                                
                            $sidebar_part = 'blog';
                            if(is_tax() || is_archive()){                                    
                                $sidebar_part = '';
                            }                               

                            if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                $sidebar_part = '';
                            }   
                                    
                            if($queried_object->taxonomy == 'category'){                                    
                                $sidebar_part = 'blog';
                            }                 
                            ?>
                            <?php get_template_part('sidebar',$sidebar_part); ?>  
                        <?php else: ?>
                            <?php get_template_part('sidebar'); ?>  
                        <?php endif; ?>                                    
                    </aside>                            
                </div><!-- end wrapper -->            
            </section><!-- end content -->

            <footer><!-- begin footer -->
                <div class="footer-content1 wrapper clearfix"><!-- begin wrapper -->                    
                    <div class="footer-logo">
						<p class="footer-slogan">傘のOEM・名入れ印刷までオリジナル傘製造なら傘総本家</p>
                        <a href="<?php bloginfo('url'); ?>/"><img alt="logo" src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.jpg" /></a>
                    </div>
                    <div class="footer-tel">
                        <img alt="tel" src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.jpg" />
                    </div>
					<div class="footer-con">
                        <a href="<?php bloginfo('url'); ?>/contact">
						    <img alt="contact" src="<?php bloginfo('template_url'); ?>/img/common/footer_con.jpg" />
						</a>
						<p class="fcontact-text">メールでのご相談やご質問はこちら</p>
                    </div>                
				</div>
				<div class="footer-content2 wrapper clearfix">								
                    <?php get_template_part('part','footernav'); ?>
					<ul class="footer-lang">
						<li><i class="fa fa-caret-right"></i><a href="<?php bloginfo('url'); ?>/#">English</a></li>
						<li><i class="fa fa-caret-right"></i><a href="<?php bloginfo('url'); ?>/#">Chinese</a></li>					
					</ul>
                </div>
                <div class="footer-copyright clearfix"><!-- begin wrapper -->
					<div class="wrapper clearfix">
						<ul class="footer-bnavi">
							<li><i class="fa fa-caret-right"></i><a href="<?php bloginfo('url'); ?>/company">運営会社</a></li>
							<li><i class="fa fa-caret-right"></i><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー </a></li>					
						</ul>
						<div class="text-copyright">Copyright (c) 2015 come across Co.,Ltd. All rights reserved.</div>
					</div>					
                </div>         
            </footer><!-- end footer -->            

        </div><!-- end wrapper -->
        <?php wp_footer(); ?>
        <!-- js plugins -->
       <script type="text/javascript">
            jQuery(document).ready(function(){
                // dynamic gNavi
                var gNavi_item_count = $('#gNavi .dynamic > li').length;
                if(gNavi_item_count > 0){
                    var gNavi_item_length = $('#gNavi .dynamic').width() / parseInt(gNavi_item_count);                    
                    $('#gNavi .dynamic > li').css('width', Math.floor( gNavi_item_length ) + 'px' )
                }                                         
            }); 			
        </script>         
    </body>
</html>
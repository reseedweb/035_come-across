<?php get_header(); ?>
	<div class="primary-row clearfix">
		<h3 class="h3-title"><?php the_title(); ?></h3>
		<?php echo do_shortcode('[contact-form-7 id="303" title="お見積り"]') ?>						
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
<script type = 'text/javascript'> 
	$( document ).ready(function() {
	$('#zipcode').change(function(){					
		//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
		AjaxZip3.zip2addr(this,'','addr','addr');
	});
	
	$('input[name="radio_typeumb"][value="子供用"]').click(function(){
		$('.hand-col1,.hand-col2').hide();
		$('.hand-col3').show();	
		$('.radio_hand .wpcf7-list-item:first').hide();
		$('.radio_hand .wpcf7-list-item').hide();
		$('.radio_hand .wpcf7-list-item:last').show();
	});
	
	$('input[name="radio_typeumb"][value="婦人用"],[value="紳士用"]').click(function(){
		$('.hand-col1,.hand-col2').show();	
		$('.hand-col3').hide();			
		$('.radio_hand .wpcf7-list-item:first').show();
		$('.radio_hand .wpcf7-list-item').show();
		$('.radio_hand .wpcf7-list-item:last').hide();
	});
			
	$('#print1').hide();					
	$('input:radio[name="radio_print"]').click(function(){
		var print_index = $('input:radio[name="radio_print"]').index(this);
		if(print_index == 0){						
			$('#print1').show();										
		}else{
			$('#print1').hide();			
		}
	});		
})
</script>
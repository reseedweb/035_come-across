<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">会社概要</h2>
		<table class="company_table">
			<tr>
				<th>社名</th>
				<td>株式会社 カムアクロス</td>
			</tr>
			<tr>
				<th>英文社名</th>
				<td>COME ACROSS CO.,LTD.</td>
			</tr>
			<tr>
				<th>所在地</th>
				<td>〒578-0982　東大阪市吉田本町1丁目11番10号</td>
			</tr>
			<tr>
				<th>TEL・FAX</th>
				<td>TEL:(072)967-2725<br />FAX:(072)967-2735</td>
			</tr>
			<tr>
				<th>設　立</th>
				<td>34820</td>
			</tr>
			<tr>
				<th>代表者</th>
				<td>代表取締役社長　今中　光昭</td>
			</tr>
			<tr>
				<th>資本金</th>
				<td>2,000万円</td>
			</tr>
			<tr>
				<th>従業員</th>
				<td>10名</td>
			</tr>
			<tr>
				<th>事業内容</th>
				<td>レイングッズ・パラソルの企画・製造・輸入・卸販売</td>
			</tr>						
		</table>
	</div><!-- end primary-row -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">アクセス</h2>
		<div id="company_map">			
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3281.1888299216953!2d135.6223909!3d34.6751834!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6001205c57b16e03%3A0xb4f5e7c4bae2237d!2s1+Chome-11-10+Yoshitahonmachi%2C+Higashi%C5%8Dsaka-shi%2C+%C5%8Csaka-fu+578-0982%2C+Japan!5e0!3m2!1svi!2s!4v1427696240115" width="760" height="300" frameborder="0" style="border:0"></iframe>
		</div>
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact');?>	
<?php get_footer(); ?>
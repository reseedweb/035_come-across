<?php get_header(); ?>		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="ln2em">
			<p>用途が決まりましたら次はそれに合ったフレーム(傘の骨)を選んで頂きます。</p>
			<p>フレームを組上げる各パーツと仕様に合わせた一般的なサイズになりますのでご参照下さい。</p>
		</div>		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">フレームを選ぶ</h3>	
		<div class="frame-content-first clearfix"><!-- begin frame-content-first -->
			<div class="frame-first-left">
				<h4 class="frame-left-title">傘のフレームの一般的名称</h4>
				<div class="frame-left-img"><img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_img1.jpg" alt="frame" /></div>
			</div><!-- end frame-first-left -->
			
			<div class="frame-first-right">
				<div class="message-group message-col129"><!-- begin message-group -->
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col">							
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_img2.jpg" alt="frame" />
							</div><!-- end image -->    
							<h4 class="frame-right-title">石突</h4>         
						</div><!-- end message-col -->
						<div class="message-col">							
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_img3.jpg" alt="frame" />
							</div><!-- end image -->    
							<h4 class="frame-right-title">上ろくろ</h4>             
						</div><!-- end message-col -->
						<div class="message-col">							
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_img4.jpg" alt="frame" />
							</div><!-- end image -->    
							<h4 class="frame-right-title">下ろくろ・バネ</h4>                
						</div><!-- end message-col -->           
					</div><!-- end message-row -->
					
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col">							
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_img5.jpg" alt="frame" />
							</div><!-- end image -->    
							<h4 class="frame-right-title">上はじき</h4>                 
						</div><!-- end message-col -->
						<div class="message-col">							
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_img6.jpg" alt="frame" />
							</div><!-- end image -->    
							<h4 class="frame-right-title">下はじき</h4>         
						</div><!-- end message-col -->
						<div class="message-col">							
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_img7.jpg" alt="frame" />
							</div><!-- end image -->    
							<h4 class="frame-right-title">留め鋲</h4>               
						</div><!-- end message-col -->           
					</div><!-- end message-row -->
					<div class="frame-right-last">
						<img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_img1.jpg" alt="frame" />
						<h4 class="frame-right-ltitle">上はじき</h4>         
					</div>
				</div><!-- end message-group -->
			</div><!-- end frame-first-right -->
		</div><!-- end frame-content-first -->

		<div class="message-right message-364 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_img9.jpg" alt="frame" />
			</div>
			<div class="text">
				<h4 class="frame-title">長傘用</h4>
				<p>長傘のフレームを組上げる各パーツと仕様に合わせた一般<br />的なサイズになりますのでご参照下さい。</p>
			</div>
		</div><!-- end message-364 -->
		
		<table class="frame-table">
			<tr>
				<th>親骨の長さ</th>
				<th>骨の数</th>
				<th>骨タイプ</th>
				<th>カラー</th>
				<th></th>
			</tr>		
			<tr>
				<td>47cm</td>
				<td>8本</td>
				<td>10mm</td>
				<td>ニッケル</td>
				<td>晴雨兼用</td>
			</tr>
			<tr>
				<td>50cm</td>
				<td>8本</td>
				<td>10mm</td>
				<td>ニッケル</td>
				<td>子供用</td>
			</tr>
			<tr>
				<td>60cm</td>
				<td>8本</td>
				<td>8mm</td>
				<td>ブラック</td>
				<td>婦人用・紳士共通</td>
			</tr>
			<tr>
				<td>60cm</td>
				<td>8本</td>
				<td>10mm</td>
				<td>ブラック</td>
				<td>紳士用</td>
			</tr>
		</table><!-- end frame-table -->
		
		<div class="message-right message-364 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_img10.jpg" alt="frame" />
			</div>
			<div class="text">
				<h4 class="frame-title">折傘用</h4>
				<p>折傘用のフレームを組上げる各パーツと仕様に合わせた<br />一般的なサイズになりますのでご参照下さい。</p>
			</div>
		</div><!-- end message-364 -->
		
		<table class="frame-table">
			<tr>
				<th>親骨の長さ</th>
				<th>骨の数</th>
				<th>骨タイプ</th>
				<th>カラー</th>
				<th></th>
			</tr>		
			<tr>
				<td>50cm</td>
				<td>6本</td>
				<td>3段丸ミニ</td>
				<td>ニッケル</td>
				<td>晴雨兼用</td>
			</tr>
			<tr>
				<td>55cm</td>
				<td>6本</td>
				<td>3段丸ミニ</td>
				<td>ブラック</td>
				<td>婦人用・紳士共通</td>
			</tr>
			<tr>
				<td>60cm</td>
				<td>6本</td>
				<td>3段丸ミニ</td>
				<td>ブラック</td>
				<td>紳士用</td>
			</tr>
		</table><!-- end frame-table -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<div class="hand-content-btn clearfix">
			<div class="hand-btn1"><a href="<?php bloginfo('url'); ?>/use"><img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_btn1.jpg" alt="frame" /></a></div>
			<div class="hand-btn2"><a href="<?php bloginfo('url'); ?>/hand"><img src="<?php bloginfo('template_url'); ?>/img/content/frame_content_btn2.jpg" alt="frame" /></a></div>
		</div><!-- hand-btn -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
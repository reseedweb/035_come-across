<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<p class="faq-top-title">お客様からよくいただく質問内容をまとめました。</p>
		<h3 class="faq-title">晴雨兼用傘は雨傘と同じように使用して可能でしょうか？</h3><!-- faq-title -->
		<div class="faq-content clearfix">
			<p>朝から雨の日の使用や、長時間の強い降雨時の使用はお避けください。</p>
			<p>生地には耐水またはっ水加工を施しておりますが、主に日傘として企画しており、<br />
			刺繍・アップリケ・レース等の特殊加工を施している商品が多く、<br />
			刺繍部分及びレース加工部分から雨がしみ込み、雨漏りする場合があります。</p>
		</div><!-- ./faq-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h3 class="faq-title">オリジナルの傘をつくりたい</h3><!-- faq-title -->
		<div class="faq-content clearfix">
			<p>まずはお問い合わせフォームよりご連絡ください。</p>
			<p>何をすれば良いのか分からなくても構いません。</p>
			<p>ご予算やロットに応じて、弊社より提案させていただくことも可能です。</p>
		</div><!-- ./faq-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h3 class="faq-title">最小ロットは何本からですか？</h3><!-- faq-title -->
		<div class="faq-content clearfix">
			<p>弊社では、最少ロット240本からのオリジナル傘の製作注文を承っております。</p>			
		</div><!-- ./faq-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h3 class="faq-title">自分でデザインしたデータで傘を製作することは可能でしょうか？</h3><!-- faq-title -->
		<div class="faq-content clearfix">
			<p>はい、可能です。</p>
			<p>お問い合わせフォームからファイルをアップロードしてください。</p>
		</div><!-- ./faq-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h3 class="faq-title">オリジナル傘や印刷の製作の日数はどれくらい掛かりますか？</h3><!-- faq-title -->
		<div class="faq-content clearfix">
			<p>約3か月程度となりますが、混雑時やシーズン中は遅れる場合がございます。</p>			
		</div><!-- ./faq-content -->
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact');?>
<?php get_footer(); ?>
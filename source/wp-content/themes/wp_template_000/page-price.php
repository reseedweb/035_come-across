<?php get_header(); ?>		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">価格例</h3>	
		<h4 class="price-title-top">パターン1</h4>
		<div class="message-group message-col177 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img1.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">形態</div>
						<div class="price-right">雨傘 婦人</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img2.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">プリント</div>
						<div class="price-right">顔料</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->	
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img3.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">骨</div>
						<div class="price-right">60x8Kジャンプ</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img4.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">手もと</div>
						<div class="price-right">合皮巻</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
			</div><!-- end message-row -->
		</div><!-- end message-group -->
		
		<table class="price-table">
			<tr>	
				<th>形態</th>
				<td colspan="3" class="price-ttop">雨傘 婦人</td>				
			<tr>
			<tr>
				<th>プリント</th>
				<td>顔料プリント</td>
				<th>生地</th>
				<td>ポンジ</td>
			</tr>
			<tr>
				<th>骨</th>
				<td>60x8Kジャンプ</td>
				<th>駒数</th>
				<td>1駒</td>
			</tr>
			<tr>
				<th>色数</th>
				<td>1色</td>
				<th>手もと</th>
				<td>合皮巻</td>
			</tr>
		</table><!-- end price-table -->
		
		<div class="price-value">
			<span class="price-icon">900円</span>
		</div><!-- end price-value -->
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<h4 class="price-title-top">パターン2</h4>
		<div class="message-group message-col177 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img5.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">形態</div>
						<div class="price-right">雨傘 婦人</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img6.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">プリント</div>
						<div class="price-right">顔料</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->	
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img7.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">骨</div>
						<div class="price-right">60x8Kジャンプ</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img8.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">手もと</div>
						<div class="price-right">合皮巻</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
			</div><!-- end message-row -->
		</div><!-- end message-group -->
		
		<table class="price-table">
			<tr>	
				<th>形態</th>
				<td colspan="3" class="price-ttop">雨傘 婦人</td>				
			<tr>
			<tr>
				<th>プリント</th>
				<td>顔料プリント</td>
				<th>生地</th>
				<td>ポンジ</td>
			</tr>
			<tr>
				<th>骨</th>
				<td>60x8Kジャンプ</td>
				<th>駒数</th>
				<td>8駒</td>
			</tr>
			<tr>
				<th>色数</th>
				<td>1色</td>
				<th>手もと</th>
				<td>合皮巻</td>
			</tr>
		</table><!-- end price-table -->
		
		<div class="price-value">
			<span class="price-icon">1,050円</span>
		</div><!-- end price-value -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<h4 class="price-title-top">パターン3</h4>
		<div class="message-group message-col177 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img9.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">形態</div>
						<div class="price-right">雨傘 婦人</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img10.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">プリント</div>
						<div class="price-right">転写</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->	
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img11.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">骨</div>
						<div class="price-right">55x6Kミニ</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img12.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">手もと</div>
						<div class="price-right">成形</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
			</div><!-- end message-row -->
		</div><!-- end message-group -->
		
		<table class="price-table">
			<tr>	
				<th>形態</th>
				<td colspan="3" class="price-ttop">雨傘 婦人</td>				
			<tr>
			<tr>
				<th>プリント</th>
				<td>転写プリント</td>
				<th>生地</th>
				<td>ポンジ</td>
			</tr>
			<tr>
				<th>骨</th>
				<td>55x6Kミニ</td>
				<th>駒数</th>
				<td></td>
			</tr>
			<tr>
				<th>色数</th>
				<td>1色</td>
				<th>手もと</th>
				<td>成形</td>
			</tr>
		</table><!-- end price-table -->
		
		<div class="price-value">
			<span class="price-icon">850<span class="price-unit">円</span></span>
		</div><!-- end price-value -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<h4 class="price-title-top">パターン4</h4>
		<div class="message-group message-col177 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img13.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">形態</div>
						<div class="price-right">雨傘 婦人</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img14.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">プリント</div>
						<div class="price-right">転写</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->	
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img15.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">骨</div>
						<div class="price-right">65x8Kジャンプ</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img16.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">手もと</div>
						<div class="price-right">成形</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
			</div><!-- end message-row -->
		</div><!-- end message-group -->
		
		<table class="price-table">
			<tr>	
				<th>形態</th>
				<td colspan="3" class="price-ttop">雨傘 婦人</td>				
			<tr>
			<tr>
				<th>プリント</th>
				<td>転写プリント</td>
				<th>生地</th>
				<td>ポンジ</td>
			</tr>
			<tr>
				<th>骨</th>
				<td>65x8Kジャンプ</td>
				<th>駒数</th>
				<td>1駒</td>
			</tr>
			<tr>
				<th>色数</th>
				<td>1色</td>
				<th>手もと</th>
				<td>成形</td>
			</tr>
		</table><!-- end price-table -->
		
		<div class="price-value">
			<span class="price-icon">1,110<span class="price-unit">円</span></span>
		</div><!-- end price-value -->
	</div><!-- end primary-row -->
	
		<div class="primary-row clearfix"><!-- begin primary-row -->			
		<h4 class="price-title-top">パターン5</h4>
		<div class="message-group message-col177 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img17.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">形態</div>
						<div class="price-right">晴雨兼用</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img18.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">プリント</div>
						<div class="price-right">転写</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->	
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img19.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">骨</div>
						<div class="price-right">47x8Kスライド</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
				
				<div class="message-col clearfix">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img20.jpg" alt="price" />
					</div><!-- end image -->				
					<div class="price-text-title">
						<div class="price-left">手もと</div>
						<div class="price-right">木</div>
					</div><!-- end price-text-title -->
				</div><!-- end message-col -->
			</div><!-- end message-row -->
		</div><!-- end message-group -->
		
		<table class="price-table">
			<tr>	
				<th>形態</th>
				<td colspan="3" class="price-ttop">晴雨兼用</td>				
			<tr>
			<tr>
				<th>プリント</th>
				<td>転写プリント</td>
				<th>生地</th>
				<td>ポンジ</td>
			</tr>
			<tr>
				<th>骨</th>
				<td>47x8Kスライド</td>
				<th>駒数</th>
				<td>8駒</td>
			</tr>
			<tr>
				<th>色数</th>
				<td>1色</td>
				<th>手もと</th>
				<td>木</td>
			</tr>
		</table><!-- end price-table -->
		
		<div class="price-value">
			<span class="price-icon">1,200<span class="price-unit">円</span></span>
		</div><!-- end price-value -->
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<div class="hand-content-btn clearfix">
			<div class="hand-btn1"><a href="<?php bloginfo('url'); ?>/print"><img src="<?php bloginfo('template_url'); ?>/img/content/price_content_btn1.jpg" alt="price" /></a></div>
			<div class="hand-btn2"><a href="<?php bloginfo('url'); ?>/estimate"><img src="<?php bloginfo('template_url'); ?>/img/content/price_content_btn2.jpg" alt="price" /></a></div>
		</div><!-- hand-btn -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
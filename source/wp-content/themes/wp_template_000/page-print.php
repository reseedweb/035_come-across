 <?php get_header(); ?>		
	<div class="primary-row ln2em clearfix"><!-- begin primary-row -->
		<p>この工程では、傘のデザインを主に選択していただきます。</p>
		<p>大きく分けて２つあり転写・顔料プリントがございます。</p>
		<p>色も多数あり下記の色を参考に選択していただくことが可能です。</p>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<h3 class="h3-title">プリントの種類</h3>
		<div class="print-slogan1">世界に1つだけのデザイン<span class="print-slogan-clr">完全オリジナル傘！</span></div>
		<div class="print-slogan2">
			<p>各種イベントや企業のプロモーション用として大好評！また、プレゼントや記念品としても最適です！</p>
			<p>さらに、同人イベントなどでの使用も可能です。デザイン制作のご相談も承りますので、お気軽にお問い合わせ下さい。</p>
		</div>
		<div class="message-right message-300 print-content clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/print_content_img1.jpg" alt="print" />
			</div>
			<div class="text">
				<h4 class="print-title1">転写プリント</h4>
				<h5 class="print-title2"><i class="fa fa-circle-o"></i>こんな方におすすめ</h5>
				<div class="print-text ln2em">
					<p><i class="fa fa-circle"></i>小ロットで作成したい</p>
					<p><i class="fa fa-circle"></i>顔料プリントに比べて安価</p>
					<p><i class="fa fa-circle"></i>デザインがカラフル(多色)でイラストや写真もプリントしたい」</p>
				</div>	
				<h5 class="print-title2 mt20"><i class="fa fa-circle-o"></i>特徴</h5>
				<div class="ln2em">
					<p>転写プリントとは転写シート(マーク)を製作し、<br />業務用ヒートプレス機でウェアに<br />熱圧着 させるプリント方式です。</p>
				</div>
			</div>
		</div><!-- end message-300 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<div class="message-right message-300 print-content mt10 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/print_content_img2.jpg" alt="print" />
			</div>
			<div class="text">
				<h4 class="print-title1">顔料プリント</h4>
				<h5 class="print-title2"><i class="fa fa-circle-o"></i>こんな方におすすめ</h5>
				<div class="print-text ln2em">
					<p><i class="fa fa-circle"></i>立体感のあるクッキリとした発色のオリジナルの傘を作成したい</p>
					<p><i class="fa fa-circle"></i>耐熱性・耐光性に優れているので晴雨兼用に最適</p>
					<p><i class="fa fa-circle"></i>耐水性に優れ色落ちしにくい</p>
					<p><i class="fa fa-circle"></i>デザインがカラフル(多色)でイラストや写真もプリントしたい</p>
				</div>					
				<h5 class="print-title2 mt20"><i class="fa fa-circle-o"></i>特徴</h5>				
			</div>						
		</div><!-- end message-300 -->
		<p class="ln2em">水にも油にも溶けないピグメント（顔料インク）に接着剤の役目をもつ合成樹脂液（バインダー） を混ぜて、プリントしたものをいいます。シルクスクリーンを張った型（型の作成方法はこちら）にインクを流し、 節に捺染した後、高温で過熱処理（ベーキング）をすると樹脂が固まり、顔料とともに繊維に固着して、洗濯しても落ちにくくなります。</p>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<div class="message-right message-300 print-content mt10 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/print_content_img3.jpg" alt="print" />
			</div>
			<div class="text">
				<h4 class="print-title1">刺繍</h4>
				<h5 class="print-title2"><i class="fa fa-circle-o"></i>こんな方におすすめ</h5>
				<div class="print-text ln2em">
					<p><i class="fa fa-circle"></i>立体感のあるクッキリとした発色のオリジナルの傘を作成したい</p>
					<p><i class="fa fa-circle"></i>耐熱性・耐光性に優れているので晴雨兼用に最適</p>
					<p><i class="fa fa-circle"></i>耐水性に優れ色落ちしにくい</p>
					<p><i class="fa fa-circle"></i>デザインがカラフル(多色)でイラストや写真もプリントしたい」</p>
				</div>					
				<h5 class="print-title2 mt20"><i class="fa fa-circle-o"></i>特徴</h5>				
			</div>						
		</div><!-- end message-300 -->
		<p class="ln2em">水にも油にも溶けないピグメント（顔料インク）に接着剤の役目をもつ合成樹脂液（バインダー） を混ぜて、プリントしたものをいいます。シルクスクリーンを張った型（型の作成方法はこちら）にインクを流し、 節に捺染した後、高温で過熱処理（ベーキング）をすると樹脂が固まり、顔料とともに繊維に固着して、洗濯しても落ちにくくなります。</p>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<h3 class="h3-title">色の種類</h3>
		<div class="print-color-content clearfix">
			<ul class="print-color-list clearfix">
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img1.jpg" alt="print" />
					<p class="print-list-text">D-010 ぼたん L</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img2.jpg" alt="print" />
					<p class="print-list-text">D-010 ぼたん R</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img3.jpg" alt="print" />
					<p class="print-list-text">D-030 あか L</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img4.jpg" alt="print" />
					<p class="print-list-text">D-030 あか R</p>
				</li>
			</ul>
			
			<ul class="print-color-list clearfix">
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img5.jpg" alt="print" />
					<p class="print-list-text">D-040 あか L</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img6.jpg" alt="print" />
					<p class="print-list-text">D-040 あか R</p>
				</li>
				<li>	
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img7.jpg" alt="print" />
					<p class="print-list-text">D-050 おれんじ L</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img8.jpg" alt="print" />
					<p class="print-list-text">D-050 おれんじ R</p>
				</li>
			</ul>
			
			<ul class="print-color-list clearfix">
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img9.jpg" alt="print" />
					<p class="print-list-text">D-060 おれんじ L</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img10.jpg" alt="print" />
					<p class="print-list-text">D-060 おれんじ R</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img11.jpg" alt="print" />
					<p class="print-list-text">D-070 き L</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img12.jpg" alt="print" />
					<p class="print-list-text">D-050 き R</p>
				</li>
			</ul>
			
			<ul class="print-color-list clearfix">
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img13.jpg" alt="print" />
					<p class="print-list-text">D-090 くさ L</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img14.jpg" alt="print" />
					<p class="print-list-text">D-090 くさ R</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img15.jpg" alt="print" />
					<p class="print-list-text">D-110 くさ L</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img16.jpg" alt="print" />
					<p class="print-list-text">D-110 くさ R</p>
				</li>
			</ul>
			
			<ul class="print-color-list clearfix">
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img17.jpg" alt="print" />
					<p class="print-list-text">D-130 あさぎ L</p>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img18.jpg" alt="print" />
					<p class="print-list-text">D-130 あさぎ R</p>
				</li>
				<li class="space1-child">
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img19.jpg" alt="print" />
					<p class="print-list-text">D-140 ぐんじょう L</p>
				</li>
				<li class="space2-child">
					<img src="<?php bloginfo('template_url'); ?>/img/content/print_color_img20.jpg" alt="print" />
					<p class="print-list-text">D-140 ぐんじょう R</p>
				</li>
			</ul>
			
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<div class="hand-content-btn clearfix">
			<div class="hand-btn1"><a href="<?php bloginfo('url'); ?>/hand"><img src="<?php bloginfo('template_url'); ?>/img/content/print_content_btn1.jpg" alt="hand" /></a></div>
			<div class="hand-btn2"><a href="<?php bloginfo('url'); ?>/price"><img src="<?php bloginfo('template_url'); ?>/img/content/print_content_btn2.jpg" alt="hand" /></a></div>
		</div><!-- hand-btn -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
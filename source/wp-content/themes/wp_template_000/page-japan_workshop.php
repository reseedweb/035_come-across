<?php get_header(); ?>		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">日本モノ工房とは</h3>
		<p><strong>傘総本家のオリジナルブランド「日本モノ工房」</strong>では、伝統的な洋傘技術で傘づくりを行っています。</p>
		<p>コンピューターには決して真似できない、微妙な木型、生地の張り合わせなど、長く使っていただいて初めてわかる「ホンモノの良さ」を感じて下さい。</p>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="workshop-content clearfix">
			<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img1.png" alt="workshop" />
			<div class="workshop-video"><iframe width="363" height="272" src="https://www.youtube.com/embed/Spws-BXU-vE" frameborder="0" allowfullscreen></iframe></div>
		</div>			
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">手作りで一つ一つに心を込めて</h3>		
		<div class="message-right message-300 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img2.jpg" alt="workshop" />
			</div>
			<div class="text">
				<p>日本モノ工房では、傘を職人の手によって一つ一つ心を込めて丁寧に製作します。</p>
				<p>傘生地を裁断するときに使用する木型もひとつひとつ職人の手作りです。</p>
				<p>左右非対称のこの木型は、熟練の職人の『感』だけで少しずつ削って張りの美しい傘を作り出していきます。</p>
			</div>
		</div><!-- end message-300 -->
		<div class="primary-row clearfix"><!-- begin primary-row -->			
			<div class="message-right message-300 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img3.jpg" alt="workshop" />
				</div>
				<div class="text">
					<h4 class="workshop-title">中綴じも丹精込めて1本1本手作りで</h4>		
					<p>縫い合わせた傘生地（カバーといっています）を傘の骨に縫いつけていきます。これを中綴じといいます。</p>
					<p>こういった細かい作業も、機械を使わず熟練した職人の手で一つ一つ、丁寧に丹精をこめて行われます。</p>
				</div>
			</div><!-- end message-300 -->
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">日本モノ工房の傘づくりの工程</h3>
		<div class="workshop-process clearfix">			
			<h4 class="workshop-process-title">木枠をあて線引きする。</h4>
			<div class="workshop-img1">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img4.jpg" alt="workshop" />
			</div>
			<div class="workshop-img2">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img5.jpg" alt="workshop" />
			</div>
		</div>			
	</div><!-- end primary-row -->

	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="workshop-process clearfix">			
			<h3 class="workshop-process-title">解れない様に１～２ミリ間隔でミシンをかける。</h3>
			<div class="workshop-img1">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img6.jpg" alt="workshop" />
			</div>
			<div class="workshop-img2">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img7.jpg" alt="workshop" />
			</div>
		</div>			
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="workshop-process clearfix">			
			<h3 class="workshop-process-title">次に８枚を縫い合わせます。</h3>
			<div class="workshop-img1">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img8.jpg" alt="workshop" />
			</div>
			<div class="workshop-img2">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img9.jpg" alt="workshop" />
			</div>
		</div>			
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="workshop-process clearfix">			
			<h3 class="workshop-process-title">丁寧に1つずつ露先を取り付けます。</h3>
			<div class="workshop-img1">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img10.jpg" alt="workshop" />
			</div>
			<div class="workshop-img2">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img11.jpg" alt="workshop" />
			</div>
		</div>			
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="workshop-process clearfix">			
			<h3 class="workshop-process-title">最後に骨を取り付けて、取れないようにしっかり留めます。</h3>
			<div class="workshop-img1">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img12.jpg" alt="workshop" />
			</div>
			<div class="workshop-img2">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img13.jpg" alt="workshop" />
			</div>
		</div>			
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="workshop-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img14.png" alt="workshop" />
		</div>		
		<div class="workshop-complete clearfix">
			<div class="workshop-comp-img1">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img15.jpg" alt="workshop" />
			</div>
			<div class="workshop-comp-img2">
				<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_content_img16.jpg" alt="workshop" />
			</div>
		</div>
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
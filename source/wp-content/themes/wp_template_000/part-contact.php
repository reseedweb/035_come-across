<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">ご注文の流れ</h2>
	<p>
		<a href="<?php bloginfo('url'); ?>/flow">
			<img src="<?php bloginfo('template_url'); ?>/img/top/top_content_flow.jpg" alt="top" />
		</a>
	</p>
	<div class="top-part-con">	
		<img src="<?php bloginfo('template_url'); ?>/img/top/top_content_con_bg.jpg" alt="top" />			
		<div class="top-part-con-btn">
			<a href="<?php bloginfo('url'); ?>/contact">		
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_content_con_btn.jpg" alt="top" />			
			</a>
		</div>
	</div><!-- ./top-part-con -->					
</div><!-- end primary-row -->
 <?php get_header(); ?>		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">傘の手もとを選ぶ</h3>		
		<div class="message-right message-300 hand-content clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/hand_content_img1.jpg" alt="hand" />
			</div>
			<div class="text">
				<h4 class="hand-title">合皮巻</h4>
				<p>高級な質感のレザー巻きハンドル。</p>
				<p>傘地などを裁断、縫製し芯木（成型品、木等）に巻きつけて製作。</p>
				<p>ファッショナブルなハンドルとしては多く用いられています。</p>
			</div>
		</div><!-- end message-300 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<div class="message-right message-300 hand-content clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/hand_content_img2.jpg" alt="hand" />
			</div>
			<div class="text">
				<h4 class="hand-title">成形</h4>
				<p>形状、重量、色彩等金型さえあれば自由に作れ、安価で大量に<br />生産できるハンドルです。</p>
				<p>安価で多彩に加工できるため広い範囲の傘に採用されています。</p>
			</div>
		</div><!-- end message-300 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<div class="message-right message-300 hand-content clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/hand_content_img3.jpg" alt="hand" />
			</div>
			<div class="text">
				<h4 class="hand-title">木</h4>
				<p>木は比較的安価な合板と自然木を使った高級なものがあります。</p>
				<p>天然木を使用した木製ハンドルや素材の形をそのまま活かした<br />寒竹ハンドルがございます。</p>
			</div>
		</div><!-- end message-300 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<div class="hand-content-btn clearfix">
			<div class="hand-btn1"><a href="<?php bloginfo('url'); ?>/frame"><img src="<?php bloginfo('template_url'); ?>/img/content/hand_content_btn1.jpg" alt="hand" /></a></div>
			<div class="hand-btn2"><a href="<?php bloginfo('url'); ?>/print"><img src="<?php bloginfo('template_url'); ?>/img/content/hand_content_btn2.jpg" alt="hand" /></a></div>
		</div><!-- hand-btn -->
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
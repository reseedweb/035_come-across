<?php get_header(); ?>
<div class="primary-row clearfix">
	<h3 class="h3-title">製品紹介</h3>
	<?php	
		$product_terms = get_terms('cat-product',array(
			'hide_empty'    => false,
			'parent' => 0,
			'orderby'           => 'slug', 
			'order'             => 'desc',    
		));
	?>
	<ul class="product-tabs"><!-- begin product-tabs-->
		<?php			
		$product_navi_term_count = 0;
		foreach($product_terms as $product_term) :
			$product_navi_term_count = $product_navi_term_count + 1; 
		?>		
			<li class="tab-link" data-tab="<?php echo $product_term->term_id;?>"><?php echo $product_term->name; ?></li>
		<?php endforeach; ?>		
	</ul><!-- end product-tabs -->

	<?php		
	$product_term_count = 0;
	foreach($product_terms as $product_term) :
		$product_term_count = $product_term_count + 1; 
	?>
	<div id="<?php echo $product_term->term_id;?>" class="product-tab-content"><!-- begin product-tab-content -->		
		<?php	
			$product_posts = get_posts( array(
				'post_type'=> 'product',				
				$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
				'posts_per_page' => 6,
				'paged' => $paged,
				'tax_query' => array(
					array(
					'taxonomy' => 'cat-product', 
					'field' => 'term_id', 
					'terms' => $product_term->term_id))	    
			));
		?>
		<div class="product-list-group"><!-- begin product-list-group -->
			<?php $i = 0;?>
			<?php foreach($product_posts as $product_post):	?>
			<?php $i++; ?>
			<?php if($i%3 == 1) : ?>
			<div class="product-list-row clearfix">		
			<?php endif; ?>	
				<div class="product-list-col product-list-col244">
					<div class="image">
						<?php echo get_the_post_thumbnail($product_post->ID,'medium'); ?>
					</div><!-- end image -->
					<div class="title">
						<?php echo $product_post->post_title; ?>
					</div><!-- end title -->    
					<div class="product-list-btn clearfix">
						<a href="<?php echo get_the_permalink($product_post->ID); ?>">商品詳細はこちら<i class="fa fa-caret-right"></i></a>
					</div>
				</div><!-- end product-list-col -->
				<?php if($i%3 == 0 || $i == count($product_posts) ) : ?>				
			</div>		
			<?php endif; ?>
			<?php endforeach; ?> 			
		</div><!-- end product-list-group -->		
	</div><!-- end product-tab-content -->
	<?php wp_reset_query(); ?>
	<?php endforeach; ?>		
	<div class="primary-row text-center">
	    <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	</div>
</div>
<?php get_template_part('part','contact'); ?>

<script type = 'text/javascript'> 
	$( document ).ready(function() {
	$('ul.product-tabs li').first().addClass("current");
	$('.product-tab-content:first').addClass("current")
	$('ul.product-tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.product-tabs li').removeClass('current');
		$('.product-tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})
})
</script>
<?php get_footer(); ?>
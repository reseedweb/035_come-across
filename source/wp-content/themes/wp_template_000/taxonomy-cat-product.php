<?php get_header(); ?>
<div class="primary-row clearfix">
	<h3 class="h3-title">製品紹介</h3>	
		<?php	
			$queried_object = get_queried_object();
			$term_id = $queried_object->term_id;
			$product_posts = get_posts( array(
				'post_type'=> 'product',				
				$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
				'posts_per_page' => 6,
				'paged' => $paged,
				'tax_query' => array(
					array(
					'taxonomy' => 'cat-product', 
					'field' => 'term_id', 
					'terms' => $term_id))	    
			));
		?>				
		<div class="product-list-group"><!-- begin product-list-group -->
			<?php $i = 0;?>
			<?php foreach($product_posts as $product_post):	?>
			<?php $i++; ?>
			<?php if($i%3 == 1) : ?>
			<div class="product-list-row clearfix">		
			<?php endif; ?>	
				<div class="product-list-col product-list-col244">
					<div class="image">
						<?php echo get_the_post_thumbnail($product_post->ID,'medium'); ?>
					</div><!-- end image -->
					<div class="title">
						<?php echo $product_post->post_title; ?>
					</div><!-- end title -->    
					<div class="product-list-btn clearfix">
						<a href="<?php echo get_the_permalink($product_post->ID); ?>">商品詳細はこちら<i class="fa fa-caret-right"></i></a>
					</div>
				</div><!-- end product-list-col -->
				<?php if($i%3 == 0 || $i == count($product_posts) ) : ?>				
			</div>		
			<?php endif; ?>
			<?php endforeach; ?> 			
		</div><!-- end product-list-group -->		
		<div class="primary-row text-center">
			<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
		</div>
		<?php wp_reset_query(); ?>
</div>
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
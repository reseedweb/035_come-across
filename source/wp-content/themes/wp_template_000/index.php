<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-first">
		<a href="<?php bloginfo('url'); ?>/japan_workshop">
			<img src="<?php bloginfo('template_url'); ?>/img/top/top_content-first.jpg" alt="top" />
		</a>
	</div>
	<p class="mt10"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img1.jpg" alt="top" /></p>
	<p class="mt20"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img2.jpg" alt="top" /></p>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">傘総本家 3つのポイント</h2>
	<div class="top-point-content">
		<ul class="top-point-list">
			<li>
				<h3 class="top-point-title">傘・日傘の企画・デザインから<br />生産まで<span class="top-point-clr">一貫生産</span></h3>
				<p class="top-point-text">傘総本家では、傘・日傘の企画からデザイン、そして生産まで<strong>すべての工程を自社のみ</strong><br />で行っています。過去の豊富な製作実績から多様なパターンの傘をご提案させて<br />いただきますので、お気軽にご相談ください。</p>
			</li>
			<li>
				<h3 class="top-point-title"><span class="top-point-clr">小ロットは、240本から</span>対応</h3>
				<p class="top-point-text">当社ではこれまで様々な企業や施設のオリジナル傘 や痛傘を制作して参りました。<br />法人のお客様はもちろん個人のお客様にも気軽にオリジナル傘をご注文頂ける様、<br />他にはない小ロット対応でなんと240本から対応させていただきます。</p>
			</li>
			<li>
				<h3 class="top-point-title"><span class="top-point-clr">自社工場で熟練の職人が製作</span>するから高品質</h3>
				<p class="top-point-text">傘総本家は日本で唯一中国に自社工場を持ち、人、技術、素材、全てを<br /><strong>「メイドインジャパン」の品質でご提供</strong>しています。<br />中国工場にも、日本から担当を派遣することで、同じ技術力で日々傘を製作しています。</p>
			</li>
		</ul><!-- ./top-point-list -->
		<div class="top-point-img">
			<img src="<?php bloginfo('template_url'); ?>/img/top/top_point_img2.png" alt="top" />
		</div><!-- ./top-point-content -->
	</div><!-- end top-point-content -->	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">初めての方へご案内</h2>
	<p><img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img3.jpg" alt="top" /></p>
	<p class="mt20">
		<a href="<?php bloginfo('url'); ?>/contact">		
			<img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img4.jpg" alt="top" />			
		</a>
	</p>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2-title">傘総本家のサービス紹介</h2>
    <div class="message-group message-col370"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col top-box-content clearfix">
				<a href="<?php bloginfo('url'); ?>/service">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/top/top_box_img1.jpg" alt="top" />
					</div><!-- end image -->
					<h3 class="title">OEM・オリジナル傘制作</h3>                    
					<div class="text">
					   自社にデザインルームを構え、デザイン、企画を<br />行っておりますので、詳細なご要望にもお応え<br />できます。オリジナルのこだわりの傘のことなら、ご相談ください。
					</div>            
				</a>				
            </div><!-- end message-col -->
            <div class="message-col top-box-content clearfix">
				<a href="<?php bloginfo('url'); ?>/product_list">
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/top/top_box_img2.jpg" alt="top" />
					</div><!-- end image -->
					<h3 class="title">取り扱い製品販売</h3>                    
					<div class="text text-box-space">
					   各種傘・子供傘・痛傘・レイングッズを取り扱って<br />います。ノベルティ用、販売用 などに<br />どうぞご注文ください 。
					</div> 
				</a>				           
            </div><!-- end message-col -->            
        </div><!-- end message-row -->
    </div><!-- end message-group -->
</div><!-- end primary-row -->

<?php get_template_part('part','contact'); ?>

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">傘総本家からのご挨拶</h2>
	<div class="message-right message-250 clearfix">
        <div class="image">
            <img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img5.jpg" alt="top" />
        </div>
        <div class="text ln2em top-info-clr">
			<p>「私たちは誇りと夢を持って傘を創り、世の中に喜びと幸せをお届けします」という理念を元にお客様に、傘を製作してまいりました。</p>
			<p>弊社は中国に自社工場を持ち、企画からデザイン、OEM生産に至るまでの<br />トータルなご依頼から、生産のみのスポット的なご依頼まで、傘の制作全般<br />にご対応いたします。</p>
			<p class="top-info-space">当社ではこれまで様々な企業や施設のオリジナル傘を製作して参りました。</p>
			<p>法人のお客様はもちろん個人のお客様にも気軽に、オリジナル傘をご注文<br />頂ける様、他にはない240本からの小ロット対応をさせていただいております。</p>
			<p class="top-info-space">雨傘、日傘、痛傘、晴雨兼用傘など、お客様のご要望やご予算に合った、<br />オリジナル傘を弊社の熟練された職人がお作りいたします。</p>
			<p class="top-info-space">オリジナルの傘を作るのが初めてのお客様でも、<br />弊社のスタッフが丁寧にサポートさせて頂きますので<br />お気軽にお問い合わせください。</p>
        </div>
    </div><!-- end message-250 -->				
</div><!-- end primary-row -->
	
<?php get_footer(); ?>